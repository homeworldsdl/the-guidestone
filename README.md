# The Guidestone

[Nix] build tools for the [Tenhauser Gate] project.

```sh
nix run 'git+https://gitlab.com/homeworldsdl/the-guidestone.git'
```


[Nix]: https://nixos.org/nix/
[Tenhauser Gate]: https://gitlab.com/homeworldsdl/TenhauserGate
