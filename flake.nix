# Copyright (C) 2023  Thibault Lemaire <thibault.lemaire@protonmail.com>
# SPDX-License-Identifier: AGPL-3.0-or-later

{
  description = "Source port of Homeworld 1";

  outputs = { self, nixpkgs }: {

    devShells.x86_64-linux.default = with nixpkgs.legacyPackages.x86_64-linux;
      let zig-build = self.packages.x86_64-linux.tenhausergate-zig-withsanitizers;
      in mkShell {
        nativeBuildInputs =
          (lib.filter (input: input != zig_0_11.hook) zig-build.nativeBuildInputs)
          ++ [
            nixpkgs-fmt
            zig
            zls
            jre8 # Java, for PlantUML
          ];
        buildInputs = zig-build.buildInputs;

        GRAPHVIZ_DOT = "${graphviz}/bin/dot"; # For PlantUML
        # With ASan enabled dlopen() fails to find libs. This is used by SDL2 to open the sound library (pipewire), and by libglvnd to open the vendor-specific OpenGL driver
        LD_LIBRARY_PATH = "${pipewire.lib}/lib;/run/opengl-driver/lib";
      };

    galactic_map = with nixpkgs.legacyPackages.x86_64-linux;
      { pname ? "TenhauserGate"
      , version ? "1.1.2"
      , src ? (fetchFromGitLab {
          domain = "gitlab.com";
          owner = "homeworldsdl";
          repo = pname;
          rev = version;
          sha256 = "sha256-WcQb+I531YFjJAAyuERwJXF8SB8IJNt0Ztyqjonm+Qo=";
        })
      , configureOptions ? ""
      }:
      stdenv.mkDerivation {
        inherit pname version src;

        buildInputs = [
          libtool
          autoconf
          automake
          gnumake
          gcc
          yacc
          flex
          SDL2
        ];

        configurePhase = ''
          cp Linux/stuff/{acinclude.m4,configure.ac,Makefile.am} .
          autoreconf -i -f
          ./configure ${configureOptions}
        '';
        buildPhase = ''
          make -j$NIX_BUILD_CORES
        '';
        installPhase = ''
          mkdir -p $out/bin
          cp src/homeworld $out/bin
        '';
      };

    packages.x86_64-linux = {
      default = self.packages.x86_64-linux.tenhausergate-autotools;

      tenhausergate-zig = nixpkgs.legacyPackages.x86_64-linux.callPackage ./zig/default.nix { };
      tenhausergate-zig-withsanitizers = nixpkgs.legacyPackages.x86_64-linux.callPackage ./zig/default.nix { withSanitizers = true; };
      tenhausergate-autotools = self.galactic_map { };
    };

    apps.x86_64-linux.default = {
      type = "app";
      program = "${self.packages.x86_64-linux.default}/bin/homeworld";
    };

  };
}
