// Copyright (C) 2023  Thibault Lemaire <thibault.lemaire@protonmail.com>
// SPDX-License-Identifier: AGPL-3.0-or-later

const c = @cImport({
    @cInclude("AITrack.h");
    @cInclude("CommandDefs.h");
    @cInclude("CommandLayer.h");
    @cInclude("MadLinkIn.h");
    @cInclude("MadLinkInDefs.h");
    @cInclude("Probe.h");
    @cInclude("StatScript.h");
    @cInclude("Universe.h");
});

const std = @import("std");
const sizeof = std.zig.c_translation.sizeof;

/// In seconds
const TIME_BEFORE_OPENING_SAILS = 3.0;
const ROTATION_THRESHOLD_VELOCITY = 100.0;

pub export var ProbeHeader: c.CustShipHeader = .{
    .shiptype = c.Probe,
    // When allocating a new ship, a region of memory is reserved for ship-specific data
    .sizeofShipSpecifics = sizeof(c.ProbeSpec),
    .CustShipStaticInit = ProbeStaticInit,
    .CustShipInit = ProbeInit,
    .CustShipHousekeep = ProbeHouseKeep,
    .CustShipStaticInitPost = null,
    .CustShipClose = null,
    .CustShipAttack = null,
    .CustShipFire = null,
    .CustShipAttackPassive = null,
    .CustShipSpecialActivate = null,
    .CustShipSpecialTarget = null,
    .CustShipRemoveShipReferences = null,
    .CustShipDied = null,
    .CustShip_PreFix = null,
    .CustShip_Save = null,
    .CustShip_Load = null,
    .CustShip_Fix = null,
};

pub export fn ProbeGetMaxVelocity(ship: *c.Ship) c.real32 {
    std.debug.assert(ship.shiptype == c.Probe);

    const static_info: *c.ShipStaticInfo = ship.staticinfo;
    if (getShipSpecifics(c.ProbeSpec, ship).HaveMoved == 0) {
        // Probe not yet dispatched, normal (slow) speed
        return static_info.staticheader.maxvelocity;
    }

    // Dispatch velocity (fast)
    return @as(*c.ProbeStatics, @ptrCast(@alignCast(static_info.custstatinfo))).ProbeDispatchMaxVelocity;
}

/// The ship-specific reserved region immediately follows (and overlaps) the `ShipSpecifics` field,
/// which is the very last field of the `Ship` struct
//
// |... common ship fields ...|sizeofShipSpecifics|ShipSpecifics|--overallocated--|
//                                                ^--  reserved memory region   --^
fn getShipSpecifics(comptime T: type, ship: *c.Ship) *T {
    std.debug.assert(ship.sizeofShipSpecifics == sizeof(T));
    return @as(*T, @ptrCast(&ship.ShipSpecifics));
}

var StaticDataByRace: [2]c.ProbeStatics = undefined;

const field_name = "ProbeDispatchMaxVelocity";
const sentinel = std.mem.zeroes(c.scriptStructEntry);
var ProbeStaticScriptTable = [_]c.scriptStructEntry{
    .{
        .name = field_name,
        .setVarCB = &c.scriptSetReal32CB,
        // The difference between the following two values is used by c.scriptSetStruct()
        // to determine the relative offset (in bytes) of the target field in the static data
        // There are two values so they could be built out of absolute pointers to an instance
        // of the struct. However Zig provides a builtin to get that relative offset,
        // so we can just use that and set offset2 to zero.
        .offset1 = @ptrFromInt(@offsetOf(c.ProbeStatics, field_name)),
        .offset2 = null,
    },
    sentinel,
};

/// Called once per race at when loading a map
pub export fn ProbeStaticInit(directory: ?[*]u8, filename: ?[*]u8, maybe_statinfo: ?*c.ShipStaticInfo) void {
    const statinfo = maybe_statinfo.?;
    const race = statinfo.shiprace;
    // There are only two races: R1 (Kushan) with index 0, and R2 (Taiidan) with index 1
    std.debug.assert(race < 2);
    const static_data = &StaticDataByRace[race];
    statinfo.custstatinfo = static_data;
    // Load tweaked static variables from `probe.shp` script
    c.scriptSetStruct(directory, filename, &ProbeStaticScriptTable, static_data);
}

/// Called once per ship
pub export fn ProbeInit(ship: ?*c.Ship) void {
    // Memory is already zero-initialised when allocated by c.univCreateShip()
    std.debug.assert(getShipSpecifics(c.ProbeSpec, ship.?).HaveMoved == c.FALSE);
}

// Only bits 0 & 2 of the `dontrotateever` field are used.
// Bit 0 is set and cleared by ships (DefenseFighter, GravWellGenerator, Probe, RepairCorvette, ResearchShip, SalCapCorvette),
// while bit 2 is used exclusively by the Command Layer.
// Together they form an OR gate as the byte is checked as a whole for zero equality
// by the AI Track layer, returning early if either bit is set
const DontRotateFlags = packed struct {
    ship_override: bool,
    _: u1,
    command_override: bool,
    __: u5,
};

/// Get the second column of the matrix
fn column2(matrix: *const c.matrix) c.vector {
    return .{
        .x = matrix.m12,
        .y = matrix.m22,
        .z = matrix.m32,
    };
}

/// Called at every universe tick when moving
pub export fn ProbeHouseKeep(maybe_ship: ?*c.Ship) void {
    const ship = maybe_ship.?;
    const probe_data = getShipSpecifics(c.ProbeSpec, ship);
    if (probe_data.HaveMoved == c.FALSE) return;

    // Spin counter-clockwise when moving fast enough
    const threshold_velocity = comptime std.math.pow(f32, ROTATION_THRESHOLD_VELOCITY, 2);
    const posinfo = &ship.posinfo;
    if (c.IS_MOVING_LINEARLY(posinfo.isMoving) != 0 and threshold_velocity < c.vecMagnitudeSquared(posinfo.velocity)) {
        // Abuse the gun tracking AI to roll the probe

        comptime std.debug.assert(sizeof(DontRotateFlags) == sizeof(@TypeOf(ship.dontrotateever)));
        const lock_rotation = @as(*DontRotateFlags, @ptrCast(&ship.dontrotateever));

        // Save current heading
        var heading: c.vector = undefined;
        c.vecCopyAndNormalize(&posinfo.velocity, &heading);
        // Get current right side (starboard)
        var right = column2(&ship.rotinfo.coordsys);
        // Rotate the up side towards the right side, and maintain heading
        lock_rotation.ship_override = false;
        _ = c.aitrackHeadingAndUp(ship, &heading, &right, 0.99);
        // prevent anything else from rotating the probe
        lock_rotation.ship_override = true;
    }

    // Open sails after some time
    if (TIME_BEFORE_OPENING_SAILS < (c.universe.totaltimeelapsed - probe_data.moveTime)) {
        // It's okay to call this again and again, it's a noop when already open or opening
        c.madLinkInOpenSpecialShip(ship);
        // Probes cannot dock. Sails will never be closed again
    }
}
