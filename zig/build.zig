// Copyright (C) 2023  Thibault Lemaire <thibault.lemaire@protonmail.com>
// SPDX-License-Identifier: AGPL-3.0-or-later

const std = @import("std");

const ParentDir = struct {
    step: std.Build.Step,
    dir: std.Build.GeneratedFile,
    file: *const std.Build.GeneratedFile,

    pub fn of(owner: *std.Build, path: *const std.Build.GeneratedFile) *ParentDir {
        const self = owner.allocator.create(ParentDir) catch @panic("OOM");
        self.* = .{
            .step = std.Build.Step.init(.{
                .id = .custom,
                .name = owner.fmt("get parent directory of {s}", .{path.step.name}),
                .owner = owner,
                .makeFn = extractParentDir,
            }),
            .file = path,
            .dir = .{ .step = &self.step },
        };
        self.step.dependOn(path.step);
        return self;
    }

    fn extractParentDir(step: *std.Build.Step, prog_node: *std.Progress.Node) !void {
        _ = prog_node;
        const self = @fieldParentPtr(ParentDir, "step", step);
        self.dir.path = std.fs.path.dirname(self.file.path.?);
        self.step.name = step.owner.fmt("get parent directory of {s}", .{self.file.path.?});
    }
};

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const sanitizers_enabled = b.option(
        bool,
        "enable_sanitizers",
        "Enable ASAN and UBSAN. Defaults to `false` as it requires additionnal runtime support libraries",
    ) orelse false;

    const bison = try b.findProgram(&.{"bison"}, &.{});
    const flex = try b.findProgram(&.{"flex"}, &.{});

    const kas2c_parser = b.addSystemCommand(&.{bison});
    const parser_src = kas2c_parser.addPrefixedOutputFileArg("--output=", "parser.c");
    const parser_header = kas2c_parser.addPrefixedOutputFileArg("--header=", "parser.h");
    kas2c_parser.addFileArg(.{ .path = "tools/kas2c/parser.y" });

    const parser_include = ParentDir.of(b, parser_header.generated);

    const kas2c_lexer = b.addSystemCommand(&.{ flex, "--case-insensitive" });
    const lexer_src = kas2c_lexer.addPrefixedOutputFileArg("--outfile=", "lexer.c");
    kas2c_lexer.addFileArg(.{ .path = "tools/kas2c/lexer.l" });

    var host_target = std.zig.CrossTarget.fromTarget(b.host.target);
    host_target.dynamic_linker = b.host.dynamic_linker;
    const kas2c = b.addExecutable(.{
        .name = "kas2c",
        .target = host_target,
        .optimize = .ReleaseSafe,
        .link_libc = true,
        .linkage = .dynamic,
    });
    kas2c.addCSourceFile(.{ .file = .{ .path = "tools/kas2c/kas2c.c" }, .flags = &.{} });
    kas2c.addCSourceFile(.{ .file = parser_src, .flags = &.{} });
    kas2c.addCSourceFile(.{ .file = lexer_src, .flags = &.{} });
    kas2c.addIncludePath(.{ .path = "src/Game/" });
    kas2c.addIncludePath(.{ .path = "tools/kas2c" });
    kas2c.addIncludePath(.{ .generated = &parser_include.dir });
    kas2c.linkSystemLibrary2("SDL2", .{ .needed = true });

    const exe = b.addExecutable(.{
        .name = "homeworld",
        .target = target,
        .optimize = optimize,
        .link_libc = true,
    });

    const arch = target.cpu_arch orelse b.host.target.cpu.arch;
    const os = target.os_tag orelse b.host.target.os.tag;
    defineMacros(exe, optimize, arch, os);

    var flags = std.ArrayList([]const u8).init(b.allocator);
    defer flags.deinit();

    // Sanitizers
    const runtime_safety_enabled = optimize == .Debug or optimize == .ReleaseSafe;
    if (runtime_safety_enabled and os == .linux) {
        if (sanitizers_enabled) {
            try flags.appendSlice(&.{
                "-fno-sanitize-trap=undefined",
                "-fsanitize=undefined,address",
                "-fno-omit-frame-pointer",
                "-fno-optimize-sibling-calls",
            });
            // These can be provided by gcc. LLVM's do not follow the same naming convention (e.g. libcompiler-rt.ubsan_minimal-x86_64.so)
            const options = .{ .needed = true, .preferred_link_mode = .Static };
            exe.linkSystemLibrary2("asan", options);
            exe.linkSystemLibrary2("ubsan", options);
        } else {
            // 2023-10-21 HWSDL definitely has UB. Until we've fixed all of it, let's disable UBSan to let the game run without trapping.
            try flags.append("-fno-sanitize=undefined");
        }
    }

    exe.linkSystemLibrary2("SDL2", .{ .needed = true });
    exe.linkSystemLibrary2("SDL2_Image", .{ .needed = true });
    exe.linkSystemLibrary2("X11", .{ .needed = true });
    exe.linkSystemLibrary2("GL", .{ .needed = true });
    exe.addIncludePath(.{ .path = "src/Game/" });
    exe.addIncludePath(.{ .path = "src/Ships/" });
    exe.addIncludePath(.{ .path = "src/SDL/" });
    exe.addIncludePath(.{ .path = "src/ThirdParty/CRC" });
    exe.addIncludePath(.{ .path = "src/ThirdParty/JPG" });
    exe.addIncludePath(.{ .path = "src/ThirdParty/LZSS" });
    exe.addIncludePath(.{ .path = "src/ThirdParty/Titan" });
    exe.addCSourceFiles(&.{
        "src/SDL/main.c",

        // Level 1 dependencies
        "src/Game/File.c",
        "src/Game/Globals.c",
        "src/Game/Memory.c",
        "src/Game/NIS.c",
        "src/Game/LOD.c",
        "src/Game/Demo.c",
        "src/Game/Captaincy.c",
        "src/Game/Debug.c",
        "src/SDL/soundlow.c",
        "src/SDL/utility.c",
        "src/Game/Key.c",
        "src/SDL/mainrgn.c",
        "src/Game/Task.c",
        "src/Game/Region.c",
        "src/Game/FEFlow.c",
        "src/Game/HorseRace.c",
        "src/SDL/render.c",
        "src/Game/FontReg.c",
        "src/SDL/mouse.c",
        "src/SDL/texreg.c",
        "src/Game/Particle.c",
        "src/Game/ColPick.c",
        "src/Game/LaunchMgr.c",
        "src/Game/BTG.c",
        "src/Game/ConsMgr.c",
        "src/Game/ResearchGUI.c",
        "src/Game/Options.c",
        "src/SDL/rinit.c",
        "src/Game/AutoLOD.c",
        "src/Game/StringSupport.c",
        "src/Game/Camera.c",

        // Level 2 dependencies
        "src/Game/TradeMgr.c",
        "src/Game/KeyBindings.c",
        "src/Game/Tweak.c",
        "src/Ships/ResearchShip.c",
        "src/Game/ShipSelect.c",
        "src/Game/CommandLayer.c",
        "src/Game/Sensors.c",
        "src/Game/Formation.c",
        "src/Game/Tutor.c",
        "src/Game/Select.c",
        "src/Game/Tactical.c",
        "src/Game/SpeechEvent.c",
        "src/Game/Undo.c",
        "src/Game/GamePick.c",
        "src/Game/CommandWrap.c",
        "src/Game/Universe.c",
        "src/Game/CameraCommand.c",
        "src/Game/GameChat.c",
        "src/Game/Vector.c",
        "src/Game/SoundEvent.c",
        "src/Game/Animatic.c",
        "src/Game/Shader.c",
        "src/SDL/screenshot.c",
        "src/Game/Tactics.c",
        "src/Game/ProfileTimers.c",
        "src/Game/Subtitle.c",
        "src/Game/TaskBar.c",
        "src/Game/KASFunc.c",
        "src/Game/Collision.c",
        "src/Game/SinglePlayer.c",
        "src/Game/InfoOverlay.c",
        "src/SDL/font.c",
        "src/Game/PiePlate.c",
        "src/Game/Transformer.c",
        "src/Game/Mesh.c",
        "src/SDL/prim3d.c",
        "src/SDL/TitanInterfaceC.c",
        "src/Game/LagPrint.c",
        "src/Game/TitanNet.c",
        "src/Game/NetCheck.c",
        "src/Game/Gun.c",
        "src/Game/ETG.c",
        "src/Game/UIControls.c",
        "src/Game/Light.c",
        "src/Game/ResearchAPI.c",
        "src/Game/ShipView.c",
        "src/Game/Ping.c",
        "src/Game/Battle.c",
        "src/Game/MultiplayerGame.c",
        "src/Game/MultiplayerLANGame.c",
        "src/Game/Alliance.c",
        "src/Game/StatScript.c",
        "src/Game/ScenPick.c",
        "src/Game/B-Spline.c",
        "src/Game/Randy.c",
        "src/Game/Trails.c",
        "src/Game/Nebulae.c",
        "src/Game/Clouds.c",
        "src/Game/Damage.c",
        "src/Game/CommandNetwork.c",
        "src/SDL/Queue.c",
        "src/Game/AutoDownloadMap.c",
        "src/Game/FEReg.c",
        "src/Game/Types.c",
        "src/SDL/rglu.c",
        "src/Game/Eval.c",
        "src/Game/Tracking.c",
        "src/Game/Color.c",
        "src/Game/Teams.c",
        "src/Game/ObjTypes.c",
        "src/Game/UnivUpdate.c",
        "src/SDL/avi.c",

        // Level 3
        "src/SDL/smixer.c",
        "src/Game/BigFile.c",
        "src/Game/AIPlayer.c",
        "src/Game/SoundEventPlay.c",
        "src/SDL/sstream.c",
        "src/Game/KAS.c",
        "src/ThirdParty/LZSS/BitIO.c",
        "src/ThirdParty/LZSS/LZSS.c",
        "src/SDL/prim2d.c",
        "src/Game/MeshAnim.c",
        "src/Game/HS.c",
        "src/Game/Blobs.c",
        "src/Game/Stats.c",
        "src/Game/SaveGame.c",
        "src/Game/Dock.c",
        "src/Game/PlugScreen.c",
        "src/Game/Twiddle.c",
        "src/Game/Objectives.c",
        "src/Game/AIVar.c",
        "src/Game/NavLights.c",
        "src/Ships/SalCapCorvette.c",
        "src/SDL/TimeoutTimer.c",
        "src/Ships/DefenseFighter.c",
        "src/Game/Scroller.c",
        "src/Game/ResCollect.c",
        "src/Game/FlightMan.c",
        "src/Game/LinkedList.c",
        "src/Ships/GravWellGenerator.c",
        "src/Game/MadLinkIn.c",
        "src/Ships/CloakGenerator.c",
        "src/Ships/DDDFrigate.c",
        "src/Ships/RepairCorvette.c",
        "src/Ships/HeavyCorvette.c",
        "src/Ships/MinelayerCorvette.c",
        "src/Game/AITrack.c",
        "src/ThirdParty/CRC/CRC32.c",
        "src/Ships/GenericDefender.c",
        "src/Game/Matrix.c",
        "src/Ships/GenericInterceptor.c",
        "src/Game/AIShip.c",
        "src/Game/Volume.c",
        "src/Game/MEX.c",
        "src/Game/LevelLoad.c",
        "src/Game/Timer.c",
        "src/SDL/fqeffect.c",
        "src/Game/AIMoves.c",
        "src/Game/AITeam.c",
        "src/Game/AIUtilities.c",
        "src/Ships/ProximitySensor.c",
        "src/Game/AIResourceMan.c",
        "src/Game/Physics.c",
        "src/Ships/DFGFrigate.c",
        "src/Ships/Mothership.c",
        "src/Game/Clipper.c",
        "src/Game/KNITransform.c",
        "src/Game/ChannelFSM.c",
        "src/Game/SoundEventStop.c",
        "src/Game/Crates.c",
        "src/Game/Chatting.c",
        "src/Game/BMP.c",
        "src/Game/Bounties.c",
        "src/Game/Star3d.c",
        "src/SDL/Titan.c",
        "src/Game/Clamp.c",
        "src/Ships/DefaultShip.c",
        "src/Ships/AdvanceSupportFrigate.c",
        "src/Ships/Carrier.c",
        "src/Ships/HeavyCruiser.c",
        "src/Ships/IonCannonFrigate.c",
        "src/Ships/LightCorvette.c",
        "src/Ships/MissileDestroyer.c",
        "src/Ships/MultiGunCorvette.c",
        "src/Ships/ResourceCollector.c",
        "src/Ships/ResourceController.c",
        "src/Ships/SensorArray.c",
        "src/Ships/StandardDestroyer.c",
        "src/Ships/StandardFrigate.c",
        "src/Ships/Drone.c",
        "src/Ships/P1IonArrayFrigate.c",
        "src/Ships/P1MissileCorvette.c",
        "src/Ships/P1Mothership.c",
        "src/Ships/P1StandardCorvette.c",
        "src/Ships/P2AdvanceSwarmer.c",
        "src/Ships/P2FuelPod.c",
        "src/Ships/P2Mothership.c",
        "src/Ships/P2MultiBeamFrigate.c",
        "src/Ships/P2Swarmer.c",
        "src/Ships/P3StandardShip.c",
        "src/Ships/FloatingCity.c",

        // Level 4
        "src/Game/Attack.c",
        "src/Game/AIHandler.c",
        "src/Game/AIEvents.c",
        "src/Game/AIOrders.c",
        "src/Game/AIDefenseMan.c",
        "src/Game/AIFleetMan.c",
        "src/Game/AIAttackMan.c",
        "src/SDL/fquant.c",
        "src/SDL/fqcodec.c",
        //"src/Ships/Probe.c",

        // Level 5
        "src/SDL/dct.c",

        // Level 6
        "src/SDL/mixfft.c",
    }, flags.items);

    const zig_sources = b.addObject(.{
        .name = "homeworld_zig",
        .root_source_file = .{ .path = "ships/Probe.zig" },
        .target = target,
        .optimize = optimize,
        .link_libc = true,
    });
    defineMacros(zig_sources, optimize, arch, os);
    zig_sources.addIncludePath(.{ .path = "src/Ships/" });
    zig_sources.addIncludePath(.{ .path = "src/Game/" });
    zig_sources.linkSystemLibrary2("SDL2", .{ .needed = true });
    zig_sources.addIncludePath(.{ .path = "src/SDL/" });
    zig_sources.addIncludePath(.{ .path = "src/ThirdParty/CRC" });
    exe.addObject(zig_sources);

    comptime var mission_idx = 1;
    inline while (mission_idx <= 16) : (mission_idx += 1) {
        transpileKAS(
            b,
            kas2c,
            exe,
            flags.items,
            b.fmt("src/Missions/SinglePlayer/Mission{:0>2}.kas", .{mission_idx}),
        );
    }
    transpileKAS(b, kas2c, exe, flags.items, "src/Missions/Tutorials/Tutorial1.kas");
    transpileKAS(b, kas2c, exe, flags.items, "src/Missions/SinglePlayer/Mission05_OEM.kas");

    // This declares intent for the executable to be installed into the
    // standard location when the user invokes the "install" step (the default
    // step when running `zig build`).
    b.installArtifact(exe);

    // This *creates* a Run step in the build graph, to be executed when another
    // step is evaluated that depends on it. The next line below will establish
    // such a dependency.
    const run_cmd = b.addRunArtifact(exe);

    // By making the run step depend on the install step, it will be run from the
    // installation directory rather than directly from within the cache directory.
    // This is not necessary, however, if the application depends on other installed
    // files, this ensures they will be present and in the expected location.
    run_cmd.step.dependOn(b.getInstallStep());

    // This allows the user to pass arguments to the application in the build
    // command itself, like this: `zig build run -- arg1 arg2 etc`
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    // This creates a build step. It will be visible in the `zig build --help` menu,
    // and can be selected like this: `zig build run`
    // This will evaluate the `run` step rather than the default, which is "install".
    const run_step = b.step("run", "Run the game");
    run_step.dependOn(&run_cmd.step);
}

fn defineMacros(step: *std.Build.Step.Compile, optimize: std.builtin.Mode, arch: std.Target.Cpu.Arch, os: std.Target.Os.Tag) void {
    // Common base flags
    step.defineCMacro("HW_GAME_HOMEWORLD", null);
    step.defineCMacro(if (optimize == .Debug) "HW_BUILD_FOR_DEBUGGING" else "HW_BUILD_FOR_DISTRIBUTION", null);

    // Arch-specific flags
    if (arch != .x86) step.defineCMacro("GENERIC_ETGCALLFUNCTION", null);
    switch (arch) {
        .x86_64 => {
            step.defineCMacro("_X86_64", null);
            step.defineCMacro("_X86_64_FIX_ME", null);
        },
        else => {},
    }

    // OS-specific flags
    if (os == .linux) step.defineCMacro("_LINUX_FIX_ME", null);
}

fn transpileKAS(
    b: *std.Build,
    kas2c: *std.Build.Step.Compile,
    exe: *std.Build.Step.Compile,
    flags: []const []const u8,
    path: []const u8,
) void {
    const preprocess = b.addSystemCommand(&.{ b.zig_exe, "cc", "-E", "-x", "c" });
    preprocess.addFileArg(.{ .path = path });
    const stem = std.fs.path.stem(path);
    const preprocessed = preprocess.captureStdOut();
    // kas2c uses its input file name to name the generated constants
    preprocess.captured_stdout.?.basename = b.fmt("{s}.kp", .{stem});
    const transpile = b.addRunArtifact(kas2c);

    transpile.addFileArg(preprocessed);
    exe.addCSourceFile(.{
        .file = transpile.addOutputFileArg(b.fmt("{s}.c", .{stem})),
        .flags = flags,
    });
    exe.addIncludePath(.{
        .generated = &ParentDir.of(
            b,
            transpile.addOutputFileArg(b.fmt("{s}.h", .{stem})).generated,
        ).dir,
    });
    exe.addCSourceFile(.{
        .file = transpile.addOutputFileArg(b.fmt("{s}.func.c", .{stem})),
        .flags = flags,
    });
}
