# Copyright (C) 2023  Thibault Lemaire <thibault.lemaire@protonmail.com>
# SPDX-License-Identifier: AGPL-3.0-or-later

{ lib
, stdenv
, fetchFromGitLab
, zig_0_11
, yacc
, flex
, pkg-config
, SDL2
, SDL2_image
, gcc-unwrapped
, withSanitizers ? false
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "TenhauserGate";
  version = "483167b90412ebcbe37d1bc5ee6d5114f86ea845";

  src = ./.;

  c_sources = fetchFromGitLab {
    domain = "gitlab.com";
    owner = "homeworldsdl";
    repo = finalAttrs.pname;
    rev = finalAttrs.version;
    hash = "sha256-jpzpJzMvoOAMo3EndCObl7yXdEhqZD/XwS7jwhxRhbo=";
  };

  nativeBuildInputs = [
    zig_0_11.hook
    yacc
    flex
    pkg-config # .linkSystemLibrary2() cannot figure out libs without this
  ];

  buildInputs = [
    SDL2
    SDL2_image
  ]
  ++ lib.optional withSanitizers gcc-unwrapped.lib;

  preBuild = ''
    ln -s ${finalAttrs.c_sources}/src ./src
    ln -s ${finalAttrs.c_sources}/tools ./tools
  '';

  zigBuildFlags = lib.optional withSanitizers "-Denable_sanitizers";
})
